#include "buttonPoll.h"

byte numBut;
pButCallback ptrButtonCallback;
tbuttonStatus buttonStatus[NUM_MAX_BUTTONS];

void buttonInit(byte numButtons, byte *butPin, bool *normalHigh, pButCallback ptrCallback)
{
    byte idx;

    numBut = numButtons;    
    ptrButtonCallback = ptrCallback;
    for (idx=0; idx<numButtons; idx++)
    {
      pinMode(butPin[idx], INPUT);
      buttonStatus[idx].pinNum = butPin[idx];
      buttonStatus[idx].normalHigh = normalHigh[idx];
      buttonStatus[idx].enabled = true;      
      buttonStatus[idx].pressPinVal = !normalHigh[idx];
      buttonStatus[idx].prevPinVal = digitalRead(butPin[idx]);
      buttonStatus[idx].butFsmState = BUT_OPEN;
      #ifdef DETECT_PERMANENT_PRESS
        buttonStatus[idx].pressTimeCnt = 0;
      #endif     
    }
}


/* Esta tarea se ejecuta en el timer tick. Para el boton dado monitorea el estado del pin y detecta si el boton esta presionado o no  */
void butDetectPress(byte butIdx)
{
    bool pinVal;

    pinVal = digitalRead(buttonStatus[butIdx].pinNum);
    
    if ( buttonStatus[butIdx].enabled)
    {     
       switch (buttonStatus[butIdx].butFsmState)
       {
         case BUT_OPEN:
          {
            /* Detectar que el boton esta siendo presionando. Si es asi, transisionar a DEBOUNCE */
            if ( pinVal == buttonStatus[butIdx].pressPinVal)
            {
              buttonStatus[butIdx].prevPinVal = pinVal;
              buttonStatus[butIdx].butFsmState = BUT_DEBOUCE;
              buttonStatus[butIdx].debounceCnt = 0;
            }
            break;
          }

          case BUT_DEBOUCE:
          {
            if ( buttonStatus[butIdx].debounceCnt == BUT_DEBOUNCE_TIME)
            {
              /* Despues de debounce, checar si se mantiene el valor del pin para cambiar de estado  */
              if ( buttonStatus[butIdx].prevPinVal == pinVal)
              {
                /* Si el valor del pin coincide con el valor definido cuando el boton es presionado, transisionar a BUT_PRESSED */ 
                if (pinVal == buttonStatus[butIdx].pressPinVal)
                {
                  buttonStatus[butIdx].butFsmState = BUT_PRESSED;
                  #ifdef DETECT_PERMANENT_PRESS
                    buttonStatus[butIdx].pressTimeCnt = 0;
                  #endif  
                  ptrButtonCallback(buttonStatus[butIdx].pinNum);           
                }
                else /* Si el valor del pin no coincide con el valor definico como cuando el boton es presionado, transisiona a BUT_OPEN*/
                {
                  buttonStatus[butIdx].butFsmState = BUT_OPEN;
                }
              }
              else /* Si el pin actual es diferente del anterior, regresamos a DEBOUNCE state */ 
              {
                buttonStatus[butIdx].prevPinVal = pinVal;
                buttonStatus[butIdx].butFsmState = BUT_DEBOUCE;
                buttonStatus[butIdx].debounceCnt = 0;
              }
            }
            else
            {
              buttonStatus[butIdx].debounceCnt++;
            }
            break;
          }

          case BUT_PRESSED:
          {
            /* Detectar que el boton no esta siendo presionando. Si es asi, transisionar a DEBOUNCE */
            if ( pinVal != buttonStatus[butIdx].pressPinVal)
            {
              buttonStatus[butIdx].prevPinVal = pinVal;
              buttonStatus[butIdx].butFsmState = BUT_DEBOUCE;
              buttonStatus[butIdx].debounceCnt = 0;
            }else
            {
              #ifdef DETECT_PERMANENT_PRESS
              if (buttonStatus[butIdx].pressTimeCnt == PERMANENT_PRESS_TIME)
              {
                 buttonStatus[butIdx].pressTimeCnt = 0;
                 ptrButtonCallback(buttonStatus[butIdx].pinNum);
              }
              else
              {
                buttonStatus[butIdx].pressTimeCnt++;
              }   
              #endif  
              
            }
            break;
          }

          default:
                {
                  buttonStatus[butIdx].butFsmState = BUT_OPEN;
                }
       }  /* switch */

    } /*if buttonStatus[butIdx].enabled*/
}

/* Por cada uno de los botones definidos, checa si el boton fue presionado */
void allButDetectPress(void)
{
  byte butIdx;

  for (butIdx = 0; butIdx <numBut; butIdx++){  
    butDetectPress(butIdx);
  }
}
