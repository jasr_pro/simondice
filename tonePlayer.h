#ifndef TONEPLAYER_H
#define TONEPLAYER_H

#include "Arduino.h"

#define BUZZER_PIN 10
#define NUM_MAX_TONE  50
#define MAX_SOUND_FREQ 20000

typedef struct{
  unsigned int freq;      /* Frecuencia en hertz */
  unsigned int duration;  /* Duracion en timer ticks */
}tSoundBeep;

typedef struct{
  bool fifoFull;
  bool fifoEmpty;
  byte rdPtr;  /* Next beep to be played (Read)*/
  byte wrPtr;  /* Slot where the next beep will be written */
  tSoundBeep toneBuffer[NUM_MAX_TONE];
  bool enablePlay;
}tTonePlayerFIFO;

void tonePlayerInit(void);
bool addTonetoPlay(tSoundBeep soundBeep);
bool enableTonePlayer(void);
bool disableTonePlayer(void);
bool tonePlayerProcess(void);
void getTonePlayerStatus(bool *isFull, bool *isEmpty);
void abortTonePlayer(void);

#endif
