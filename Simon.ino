#include "TimerOne.h"
#include "tonePlayer.h"
#include "buttonPoll.h"

/*
  Este es el juego de SIMON dice!
  - Es un juego de memoria que consiste en recordar una secuencia de luces. 
  - El juego contiene 4 leds y 4 botones asociados entre si. La secuencia se mostrara con los leds y para repetirla se 
    usaran los botones. El juego tiene 10 niveles (este nivel puede configurarse). 
  - Al comenzar el juego se escuchara una melodia inicial como beinvenida al juego. La melodia esta acompanada con luces secuenciales.
  - Una vez terminada la melodia inicial, se necesita que el jugador presione algun boton para comenzar el juego.
  - El sistema encendera un led y el usario tendra que presionar el boton asociando al  teniendo 2 segundos de tiempo limite para hacerlo.
  - Si el tiempo limite se excede el juego termina y el jugador pierde.
  - Si el jugador acierta, se brincara al siguiente nivel en el que se agregara un nuevo led encendido a la secuencia. Al terminar la 
    secuencia el jugador debera reproducirla.
  - Si el jugadro acierta toda la secuencia llegando al nivel maximo, el juego termina y el jugador gana.
  - El jugador sabra que gano con una melodia ganadora acompanada de luces secuenciales. Asi mismo
    sabra que perdio con una secuencia de 3 beeps de baja frecuencia y luces prendiendo y apagando en todos los leds.
  - Una vez terminado el juego, el juego se puede reiniciar presionando algun boton.
  - El juego cuenta un modo de debug (si la definicion en el codigo tiene la definicion DEBUG) en donde se mostraran mensajes en la consola
    serial.

*/

/* Definicion para definir el tono inicial */
#define INIT_LED_KIT 
#define TIMER_TICK 10000  /* Timer tick 10 ms en us*/

#define DEBUG  /* Usado para agregar mesnajes de debug */

#define NUM_LEDS_BUT 4

#define BOTON1 2
#define BOTON2 3
#define BOTON3 4
#define BOTON4 5

#define LED1 6
#define LED2 7
#define LED3 8
#define LED4 9

#define FREQ1 300
#define FREQ2 400
#define FREQ3 500
#define FREQ4 600


#define SIMON_NUM_STAGES 10                  /* Numero de niveles para ganar */
#define SIMON_SHOW_LED_TONE_TIME_TICKS  75   /* 750 ms. Tiempo que se mostrara el led encendido y el tono en cada itreracion */
#define SIMON_PAUSE_AFTER_SHOW_TIME_TICKS 20  /* 200 ms de pause entre nivel y nivel cuando muestra el led/tone */
#define SIMON_TIMEOUT_TO_PLAY_TICKS 200       /* 2 segundos para que el jugador presione algun boton */
#define SIMON_INTERLEVEL_PAUSE_TICKS  100     /* Espera de 1 segundo antes de cambiar de nivel */

#define MS_TICK_INVERT  (TIMER_TICK/1000)
#define TONE_TIME(len) (len/MS_TICK_INVERT)  /* En ms*/


/* RickRoll: Tomado de https://playground.arduino.cc/Main/RickRoll */
#define  c4s    277     // 277 Hz
#define  e4f    311     // 311 Hz    
#define  f4     349     // 349 Hz 
#define  a4f    415     // 415 Hz  
#define  b4f    466     // 466 Hz 
#define  b4     493     // 493 Hz 
#define  c5     523     // 523 Hz 
#define  c5s    554     // 554 Hz
#define  e5f    622     // 622 Hz  
#define  f5     698     // 698 Hz 
#define  f5s    740     // 740 Hz
#define  a5f    831     // 831 Hz 
#define rest    65535

const tSoundBeep initPlay[] = {
   /*RickRoll*/
   {c5s,TONE_TIME(600)}, {e5f,TONE_TIME(1000)}, {e5f,TONE_TIME(600)}, {f5,TONE_TIME(600)},   {a5f,TONE_TIME(100)}, {f5s,TONE_TIME(100)}, 
   {f5,TONE_TIME(100)},  {e5f, TONE_TIME(100)}, {c5s,TONE_TIME(600)}, {e5f,TONE_TIME(1000)}, {rest,TONE_TIME(400)}, {a4f,TONE_TIME(200)}, 
   {a4f,TONE_TIME(1000)}

   /* Otro tono inicial */
#if 0  
  {300, TONE_TIME(500)}, {400, TONE_TIME(500)}, {500, TONE_TIME(500)}, {200, TONE_TIME(500)}, {250, TONE_TIME(500)},
  {300, TONE_TIME(500)}, {400, TONE_TIME(500)}, {500, TONE_TIME(500)}, {200, TONE_TIME(500)}, {250, TONE_TIME(500)},
#endif
};
byte  initToneLen = sizeof(initPlay)/sizeof(tSoundBeep);

const tSoundBeep goodPlay[] = {
  {500, TONE_TIME(400)}, {700, TONE_TIME(200)}, {1000, TONE_TIME(200)}, {500, TONE_TIME(100)}, {700, TONE_TIME(300)}, {30000, TONE_TIME(100)},
  {500, TONE_TIME(400)}, {700, TONE_TIME(200)}, {1000, TONE_TIME(200)}, {500, TONE_TIME(100)}, {700, TONE_TIME(300)}, {30000, TONE_TIME(100)},
  {500, TONE_TIME(400)}, {700, TONE_TIME(200)}, {1000, TONE_TIME(200)}, {500, TONE_TIME(100)}, {700, TONE_TIME(300)}, {30000, TONE_TIME(100)}
    
}; 
byte  goodToneLen = sizeof(goodPlay)/sizeof(tSoundBeep);

const tSoundBeep badPlay[] = {
  {100, TONE_TIME(300)}, {30000, TONE_TIME(50)}, {100, TONE_TIME(300)}, {30000, TONE_TIME(50)}, {100, TONE_TIME(300)}
};
byte  badToneLen = sizeof(badPlay)/sizeof(tSoundBeep);

typedef enum{
  INIT,       /* Estado inicial */ 
  PLAY_GEN,   /* Genera un nuevo led en la secuencia */
  PLAY_SET,   /* Muestra un led/tono en la secuencia */
  PLAY_SET_SHOW,  /* Tiempo mientras se muestra el led/tono en la secuencia */ 
  PLAY_SET_PAUSE, /* Hace una pausa entre etapas  */
  PLAY_GET,       /* Espera por el jugador presione un boton en la secuencia */
  PLAY_GET_SHOW, /* Hace una pausa para mostrar el led/tono del boton que presiono el jugador */
  PLAY_LEVEL_PAUSE,
  END_WIN,    /* El jugador gano este juego */
  END_LOST,    /* El jugador perdio este juego */
  END_WAIT     /* Espera final despues de reinicar el juego */
}tStateSimonEnum;

typedef struct{ 
  bool timerTick10ms;  /* Bandera indicando que el timer expiró */ 
  bool initDone;       /*  Inicializacion completa */
  unsigned char randCnt; /* Contador de ticks para randomizacion */
  tStateSimonEnum state; /* Estado del juego */
  bool buttonLocked;     /* Bandera para bloquear la ejecucion de la funcion de callback de boton cuando se detecta que se presiono*/
  byte buttonPressedNum; /* Boton que fue presionado */
  bool buttonPressed;    /* Bandera que indica que el boton fue presionado */
  bool buttonPressedEarly; /* bandera que indica que el boton fue presionado en una etapa previa (PLAY_GET_SHOW)*/
  byte stage;            /* Nivel del juego */ 
  byte stageIterator;    /* Iterador para barrer los niveles del juego */       
  byte stageRand[SIMON_NUM_STAGES];  /* Memoria de los leds que se encienden en cada nivel del juego. Se fija en base 0 */
  unsigned int timeOutCnt;          /* Contador de timeout para presionar algun boton */
  unsigned int timePauseCnt;        /* Contador de pausa entre etapas */
  unsigned int timeShowCnt;         /* Contador para mostrar el led encendido y timbrar en cada etapa*/
  bool restartGame;
}tStatusSimon;


/*Declaracion de funciones */
void Config_timer1(void);
void timerCallBack(void);
void apagaLeds(void);
void enciendeLeds(void);
void kitSequence(void);
void toggleSequence(void);

unsigned char getRandLed();
void randomLedSequence();
void buttonCallback(byte button);
void mainSimonProcess(void);

/* Variable definition */
tSoundBeep soundBeep;
tStatusSimon statusSimon;

byte butPin[NUM_LEDS_BUT] = {BOTON1, BOTON2, BOTON3, BOTON4};
byte ledPin[NUM_LEDS_BUT] = {LED1, LED2, LED3, LED4};
unsigned int freqPin[NUM_LEDS_BUT] = {FREQ1, FREQ2, FREQ3, FREQ4};
bool butNormalHigh[NUM_LEDS_BUT] = {false, false, false, false}; /* Botones normalmente entregan un nivel bajo */

void setup() {
  byte i;

  #ifdef DEBUG
  Serial.begin(9600);
  Serial.println("===== SIMON DICE!!! ======");
  #endif 
  statusSimon.initDone = false;
  statusSimon.state = INIT;
  statusSimon.stageIterator = 0;
  // put your setup code here, to run once:

  buttonInit(NUM_LEDS_BUT, butPin, butNormalHigh, &buttonCallback); 

  /* Inicializacion de LEDs */ 
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);  
  
  digitalWrite(LED1, 0);
  digitalWrite(LED2, 0);
  digitalWrite(LED3, 0);
  digitalWrite(LED4, 0);

  Config_timer1();
  tonePlayerInit();

  /* Inicializando el tono Inicial */
  for (i=0; i<initToneLen; i++)
  {
    if (!addTonetoPlay(initPlay[i]))
      break;
  }

  /* Repitiendo el tono Inicial */
  for (i=0; i<initToneLen; i++)
  {
    if (!addTonetoPlay(initPlay[i]))
      break;
  }

  /* Habilitar el Tone player */
  enableTonePlayer();
}

void loop() {
  static bool tonePlayingPrev = false;
  bool tonePlaying = false;
  bool newTone = false;
  bool tonePlayerFull = false;
  bool tonePlayerEmpty = false;
  
  // put your main code here, to run repeatedly:

  /* Codigo executado a 10ms rate */
  if (statusSimon.timerTick10ms)
  {
    /* Proceso para que el tone player funcione. Esta funcion regresa true si actualmente se esta tocando algun tono */
    tonePlaying = tonePlayerProcess();

    /* Detecta si un nuevo tono ha comenzado (1er tick). Se usa para controlar el encendido de los leds cuando se tocal el tono de inicializacion*/
    if ((tonePlayingPrev == false) && (tonePlaying == true))
    {
      newTone = true;
    }
    else
    {
      newTone = false;
    }
    tonePlayingPrev = tonePlaying;

    /* Checando que la inicializacion termino. Esto se da cuando el tone player esta vacio y ya no esta tocando */
    getTonePlayerStatus(&tonePlayerFull, &tonePlayerEmpty);
    if ((!statusSimon.initDone) && (tonePlayerEmpty) && (!tonePlaying))
    {
      statusSimon.initDone = true;
      apagaLeds();
    }

    if ((statusSimon.state == END_WAIT) && (tonePlayerEmpty) && (!tonePlaying))
    {
      statusSimon.restartGame = true;
      apagaLeds();
    }
    /* Sequencia de leds durante la inicializacio */
    if ((!statusSimon.initDone) && (newTone)){
      #ifdef INIT_LED_KIT
        kitSequence();
      #else 
        randomLedSequence(); 
      #endif
    }

    if (( statusSimon.state == END_WAIT ) && (newTone))
    {
      toggleSequence();
    }

   /* Detectar si los botones se han presionado. En caso de que asi sea, la funcion de callback se ejecutara */
   statusSimon.buttonLocked = false;  /* Desbloqueando la identificacion del boton que fue presionado. Se bloquea para 
                                         solo marcar un solo boton presionado (el primero) */  
                                         
   /* Deteccion de botones presionados se hace solo despues de haber terminado la inicializacion */
   if (statusSimon.initDone)
   {
     allButDetectPress();
   }

    /* Proceso principal del juego */
    mainSimonProcess();
    statusSimon.timerTick10ms = false;
  }
  
}

/* Eniende un LED */
void enciendeLED(byte led)
{
  if ((led == LED1) || (led == LED2) || (led == LED3) || (led == LED4))
  {  
    digitalWrite(led, 1);
  }
}

/*  Apaga todos los LEDs */
void apagaLeds(void)
{
  digitalWrite(LED1, 0);
  digitalWrite(LED2, 0);
  digitalWrite(LED3, 0);
  digitalWrite(LED4, 0);
}

/*  Enciende todos los LEDs */
void enciendeLeds(void)
{
  digitalWrite(LED1, 1);
  digitalWrite(LED2, 1);
  digitalWrite(LED3, 1);
  digitalWrite(LED4, 1);
}


void kitSequence(void)
{
  static byte state = 0;
   apagaLeds();
   switch (state)
   {
    case 0: {state = 1;}
    case 1: {enciendeLED(LED1); state = 2;  break;}
    case 2: {enciendeLED(LED2); state = 3;  break;}
    case 3: {enciendeLED(LED3); state = 4;  break;}
    case 4: {enciendeLED(LED4); state = 5;  break;}
    case 5: {enciendeLED(LED3); state = 6;  break;}
    case 6: {enciendeLED(LED2); state = 7;  break;}
    case 7: {enciendeLED(LED1); state = 2;  break;}
    default: state = 0;    
   }
}

/* Secuencia de encendido y apagado de leeds */
void toggleSequence(void)
{
  if (digitalRead(LED1))
  {
    apagaLeds();
  }
  else
  {
    enciendeLeds();
  }
}


/* Obtinene un numero aleatorio desde 0 hasta 3 usando el contador de timer tick */
unsigned char getRandLed(){
  
  unsigned char ledButBits = statusSimon.randCnt &  0x3; /* Con 2 bits se puede acceder hasta 4 leds/botones */
  return ledButBits;
}

/* Genera una sequencia aleatoria de encendido de leds (usando durante la inicializacion) */
void randomLedSequence(){

  unsigned char randLed;

  randLed = getRandLed();
  apagaLeds();
  
  switch (randLed)
  {
    case 0: {enciendeLED(LED1); break;}
    case 1: {enciendeLED(LED2); break;}
    case 2: {enciendeLED(LED3); break;}
    case 3: {enciendeLED(LED4); break;}
    default: apagaLeds();
  }
  
}

//--------------------- Timer configuration ----------------------------
void Config_timer1(void){
    Timer1.initialize(TIMER_TICK);
    Timer1.attachInterrupt( timerCallBack, TIMER_TICK);
    statusSimon.timerTick10ms = false;
}

//--------------------- Timer Callback ----------------------------
void timerCallBack(void)
{
  statusSimon.timerTick10ms = true;
  statusSimon.randCnt++;
}


/*Funcion callback para ejecutarse una vez que algun boton se presione */
void buttonCallback(byte button)
{
  /* Solo los botones son validos si esta en los estados INIT y PLAY_GET */
  if (((statusSimon.state == INIT) || (statusSimon.state == PLAY_GET) || (statusSimon.state == PLAY_GET_SHOW)) && (statusSimon.buttonLocked == false))
  {
      statusSimon.buttonPressedNum = button;
      statusSimon.buttonPressed = true;
      statusSimon.buttonLocked = true;  /* Se bloquea en caso de que el callback se ejecute mas de una vez en 1 tick. 
                                           Solo se identificara el 1er boton presionado. Se desbloqueare en el siguiente tick */
  }      
}

/* Funcion principal donde se ejecutara el juego de Simon. Dependiendo del estado actual, se generara aleatoriamente una sequencia de leds,
   se leera el estado de los botones, se tocara un tono cada vez que se presione un boton y se avanzara en la secuencia de juego en cada iteracion */
void mainSimonProcess(void)
{
 tSoundBeep soundBeep;
 byte idx;
 
 switch (statusSimon.state)
 {
  case INIT:
    {       
       if ( statusSimon.buttonPressed )
       {
         #ifdef DEBUG
         Serial.println("A jugar ...");
         #endif
         
         /* Empieza el juego */
         statusSimon.stage = 0;          /* Inicializando en 0 el nivel de juego */
         statusSimon.stageIterator = 0;
         statusSimon.state = PLAY_GEN;
       }
       break;
    }

   /* Se generara alatoriamente un numero de boton*/
   case  PLAY_GEN:
   {
     /* Aleatorizacion genera un numero en base 0. Se tiene que convertir a LED y BOTON para su uso posterior */
     statusSimon.stageRand[statusSimon.stage] = getRandLed();     
     #ifdef DEBUG
       Serial.print("Nivel ");
       Serial.print(statusSimon.stage);
       Serial.print(" - Simón dice LED: ");       
       Serial.println(statusSimon.stageRand[statusSimon.stage]); 
     #endif
     statusSimon.stage++; 
     statusSimon.stageIterator = 0;
     statusSimon.state = PLAY_SET;  /* Moverse a que se muestre la sequencia de numeros hasta ahora */
   }

   /* Muestra la sequencia de numeros seleccionada hasta ahora */
   case  PLAY_SET:
   {               
     idx = statusSimon.stageRand[statusSimon.stageIterator];
     soundBeep.freq = freqPin[idx];
     soundBeep.duration = SIMON_SHOW_LED_TONE_TIME_TICKS;
     addTonetoPlay(soundBeep);
     enciendeLED(ledPin[idx]);
     statusSimon.timeShowCnt = 0;
     statusSimon.state = PLAY_SET_SHOW;
     break;
   }

  case PLAY_SET_SHOW:
  {
    /* Si se cumplio el tiempo para mostrar los leds y sonido, hacer una pausa si hay que mostrar led/tone de otra etapa o brincar directo a que 
       el jugador entre su sequencia */
    if (statusSimon.timeShowCnt == SIMON_SHOW_LED_TONE_TIME_TICKS )
    { 
      apagaLeds();
      statusSimon.stageIterator++; 
      if (statusSimon.stageIterator == statusSimon.stage)
      {
        statusSimon.timeOutCnt = 0;
        statusSimon.stageIterator = 0;
        statusSimon.state = PLAY_GET;
      }
      else
      {
        statusSimon.timePauseCnt = 0;        
        statusSimon.state = PLAY_SET_PAUSE;
      }
    }else{
      statusSimon.timeShowCnt++;
    }
    break;
  }
  
  case PLAY_SET_PAUSE:
  {
    if (statusSimon.timePauseCnt == SIMON_PAUSE_AFTER_SHOW_TIME_TICKS )
    {               
      statusSimon.state = PLAY_SET;            
    }
    else{
      statusSimon.timePauseCnt++;
    }
    break;
  }

  case PLAY_GET:
  {
    byte button;
    if ( statusSimon.buttonPressed || statusSimon.buttonPressedEarly  )
    {
      /* Checa si el boton presionado coincide con el boton en la secuencia en esta iteracion */
      idx = statusSimon.stageRand[statusSimon.stageIterator];
      button = butPin[idx];
      if ( statusSimon.buttonPressedNum == button )
      {
       #ifdef DEBUG
       Serial.println(" Correcto!! "); 
       #endif 
        /* Si coicide el boton y estamos en el utlimo nivel, el juego se acaba y el jugador gana.
           En este punto el iterador va de 0 a stageIterator-1 */        
        if ((statusSimon.stageIterator == (statusSimon.stage-1)) && (statusSimon.stage == SIMON_NUM_STAGES))
        {
          #ifdef DEBUG
          Serial.println("GANASTE!!");
          #endif
          statusSimon.state = END_WIN;
        }
        else
        {
          soundBeep.freq = freqPin[idx];
          soundBeep.duration = SIMON_SHOW_LED_TONE_TIME_TICKS;
          addTonetoPlay(soundBeep);
          enciendeLED(ledPin[idx]);        
          statusSimon.timeShowCnt = 0;
          statusSimon.state = PLAY_GET_SHOW;
        }   
      }
      else
      {
        #ifdef DEBUG
        Serial.println("PERDISTE!!");
        #endif
        statusSimon.state = END_LOST;
      }
    }
    else
    {
      /* Checando por timeout */
      if (statusSimon.timeOutCnt == SIMON_TIMEOUT_TO_PLAY_TICKS)
      {
        /* Timeout */
        Serial.println("Tardaste mucho, PERDISTE!!");                
        statusSimon.state = END_LOST;
      }
      else
      {
        statusSimon.timeOutCnt++;
      }
    }

    if (statusSimon.buttonPressedEarly)
    {
      statusSimon.buttonPressedEarly = false;          
    }
    break;
  }

  case PLAY_GET_SHOW:
  {
    if (( statusSimon.buttonPressed ) || (statusSimon.timeShowCnt == SIMON_SHOW_LED_TONE_TIME_TICKS))
    {
      apagaLeds();
      statusSimon.stageIterator++;
      if (statusSimon.buttonPressed)
      {
        abortTonePlayer(); 
        statusSimon.buttonPressedEarly = true;
      }
      /* Si ya llego al nivel en el que esta el juego  revisa si ya se termino el juego, si no regresar*/
      if (statusSimon.stageIterator == statusSimon.stage)
      {
        statusSimon.state = PLAY_LEVEL_PAUSE;
        statusSimon.timePauseCnt = 0;
      }
      else
      {
        statusSimon.state = PLAY_GET;
        statusSimon.timeOutCnt = 0;
      }
    }else
    {
      statusSimon.timeShowCnt++;
    }
    /* Se puede salir de este estado si se presiona algun otro boton*/
    break;
  }

  case PLAY_LEVEL_PAUSE:
  {
    if (statusSimon.timePauseCnt == SIMON_INTERLEVEL_PAUSE_TICKS)
    {
      statusSimon.state = PLAY_GEN;
    }
    else
    {
      statusSimon.timePauseCnt++;
    }
    break;
  }

  case END_WIN:
  {
    apagaLeds();
    for (idx=0; idx<goodToneLen ; idx++)
    {
      if (!addTonetoPlay(goodPlay[idx]))
        break;
    }
    statusSimon.state = END_WAIT;  
    break;
  }
  
  case END_LOST:
  {
    apagaLeds();
    for (idx=0; idx<badToneLen ; idx++)
    {
      if (!addTonetoPlay(badPlay[idx]))
        break;
    }
    statusSimon.state = END_WAIT;
    break;      
  }

  case END_WAIT:
  {
    if (statusSimon.restartGame)
    {
      #ifdef DEBUG
      Serial.println("El juego termino. Presiona un boton para volver a jugar."); 
      Serial.println("-------------------------------------------------------");
      #endif
      statusSimon.state = INIT;
      statusSimon.restartGame = false;
    }
    break;
  }
 }

 
 /* Borrar la bandera de boton presionado si es que prendida*/
 if ( statusSimon.buttonPressed )
 {
   statusSimon.buttonPressed = false;
 }
}
