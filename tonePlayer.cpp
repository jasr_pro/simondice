#include "tonePlayer.h"

bool getToneFromFifo(tSoundBeep *soundBeep);

tTonePlayerFIFO tonePlayerFifo;

void tonePlayerInit(void)
{
  tonePlayerFifo.fifoFull = false;
  tonePlayerFifo.fifoEmpty = true;
  disableTonePlayer();
  tonePlayerFifo.rdPtr = 0;
  tonePlayerFifo.wrPtr = 0;
}

/* Add to fifo a new tone to be played */
bool addTonetoPlay(tSoundBeep soundBeep)
{
  bool resp;
  /* Si la memoria esta llena, responde que fallo con un FALSE*/
  if ( tonePlayerFifo.fifoFull )
  {
    resp = false;
  }else
  {
    resp = true;
    tonePlayerFifo.fifoEmpty = false;
    tonePlayerFifo.toneBuffer[tonePlayerFifo.wrPtr] = soundBeep;
    tonePlayerFifo.wrPtr++;
    if (tonePlayerFifo.wrPtr == NUM_MAX_TONE){  /* Desborde del apuntador */
      tonePlayerFifo.wrPtr = 0;
    }
    if (tonePlayerFifo.wrPtr == tonePlayerFifo.rdPtr)  /* Si los punteros se alcanzan, es indicacion de que la FIFO esta llena */
    {
      tonePlayerFifo.fifoFull = true;
    }
  }  
  return resp;
}

/* Get from FIFO the next beep to be played */
bool getToneFromFifo(tSoundBeep *soundBeep)
{
 bool resp; 
 /* La FIFO esta vacia, por lo tanto no sacar nada*/
 if (( tonePlayerFifo.rdPtr == tonePlayerFifo.wrPtr) && (!tonePlayerFifo.fifoFull))
 {
   resp = false;
   tonePlayerFifo.fifoEmpty = true;
 }
 else
 {
   resp = true;  
   *soundBeep = tonePlayerFifo.toneBuffer[tonePlayerFifo.rdPtr];
   tonePlayerFifo.rdPtr++;
   if (tonePlayerFifo.rdPtr == tonePlayerFifo.wrPtr){
     tonePlayerFifo.fifoEmpty = true;  
   }
   if (tonePlayerFifo.rdPtr == NUM_MAX_TONE)
   {
     tonePlayerFifo.rdPtr = 0;
   }
   tonePlayerFifo.fifoFull = false;
 }
 return resp;
}


/* Habilita tone player */
bool enableTonePlayer(void)
{
  tonePlayerFifo.enablePlay = true;
}

/* Deshabilita tone player */
bool disableTonePlayer(void)
{
  tonePlayerFifo.enablePlay = false;
}

/* Revisa si hay tonos disponibles en la FIFO, si es el caso, los toca por el tiempo establecido. 
   La funcion se necesita ejecutar por el tiempo indicado. Regresa true si hay un tono siendo tocado.
   Entre tono y tono hay una pausa de 1 tick, en este punto la funcion regresa false.*/
bool tonePlayerProcess(void)
{
  static unsigned int timeCnt = 0;
  static bool playingNow = false;
  tSoundBeep soundBeep;
  
  if (tonePlayerFifo.enablePlay)
  {
    if (!playingNow)
    {
      /* Si no esta tocando nada ahora*/
      if ( getToneFromFifo(&soundBeep))
      {  
        timeCnt = 0;
        playingNow = true; 
      }      
    }
    if (playingNow)
    {
      if (soundBeep.duration == timeCnt)
      {
        noTone(BUZZER_PIN);
        playingNow = false;
      }else{
        if (soundBeep.freq <= MAX_SOUND_FREQ)
        { 
          tone(BUZZER_PIN, soundBeep.freq);
        }
        else{
          noTone(BUZZER_PIN);
        }
        timeCnt++;
      }
    }        
  }

  return playingNow;
}

/* Regresa el estatus de la FIFO del tone player */
void getTonePlayerStatus(bool *isFull, bool *isEmpty){
   *isFull  = tonePlayerFifo.fifoFull;
   *isEmpty = tonePlayerFifo.fifoEmpty;
}

/* Aborta la ejecucion de tonos.  */
void abortTonePlayer(void){
  tonePlayerFifo.fifoFull = false;
  tonePlayerFifo.fifoEmpty = true;  
  tonePlayerFifo.rdPtr = tonePlayerFifo.wrPtr;
  noTone(BUZZER_PIN); 
}
