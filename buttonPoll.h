#ifndef BUTTONPOLL_H
#define BUTTONPOLL_H

#include "Arduino.h"

#define NUM_MAX_BUTTONS 5    /* Soporta hasta este numero de botones */
#define BUT_DEBOUNCE_TIME 10    /* 100 ms, Assumming a tick time of 10 ms */

/*#define DETECT_PERMANENT_PRESS  */  /* Usado para detectar de manera continua que el boton ha sido presionado y sigue siendo presionado */

#ifdef DETECT_PERMANENT_PRESS
  #define PERMANENT_PRESS_TIME   30 /* Tiempo usado para muestrar el boton cuando esta presionado */
#endif

typedef enum{
  BUT_OPEN,
  BUT_DEBOUCE,
  BUT_PRESSED
}tbutFsmState;

typedef struct{
  bool enabled;
  byte pinNum;
  bool normalHigh;    /* True si por default el pin esta en alto o false si por default el pin esta en bajo*/
  byte debounceCnt; 
  #ifdef DETECT_PERMANENT_PRESS
  byte pressTimeCnt;
  #endif 
  bool prevPinVal;    /* Valor del pin antes del debounce */
  bool pressPinVal;
  tbutFsmState butFsmState;   /* Dependiendo de normalHigh este valor indicara si se detecta que el boton esta presionado o no*/
}tbuttonStatus;

typedef void (*pButCallback)(byte button);

void buttonInit(byte numButtons, byte *butPin, bool *normalHigh, pButCallback ptrCallback);
void allButDetectPress(void);

#endif
